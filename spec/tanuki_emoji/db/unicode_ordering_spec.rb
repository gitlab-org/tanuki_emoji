# frozen_string_literal: true

require 'rspec'

RSpec.describe TanukiEmoji::Db::UnicodeOrdering do
  it 'populates indexed emojis with unicode ordering data' do
    expect(TanukiEmoji.index.all.none? { |item| item.sort_key.nil? }).to be_truthy
  end
end
