# frozen_string_literal: true

require 'rspec'

RSpec.describe TanukiEmoji::Db::EmojiTestParser do
  describe '#data' do
    subject(:emoji_data) { described_class.new(index: TanukiEmoji.index).data }

    before do
      TanukiEmoji.index.reset!(reload: false)
    end

    describe 'returns an array of EmojiData' do
      it { is_expected.to be_a(Array) }
      it { is_expected.to all(be_an(TanukiEmoji::Db::EmojiTestData)) }
    end

    context 'when each EmojiTestData item' do
      it 'has codepoints filled' do
        emoji_data.each do |data|
          expect(data.codepoints).not_to be_empty
        end
      end

      it 'has emoji filled' do
        emoji_data.each do |data|
          expect(data.emoji).not_to be_empty
        end
      end

      it 'has version filled and following a known version pattern' do
        emoji_data.each do |data|
          expect(data.version).not_to be_empty
          expect(data.version).to match(/[0-9]+\.[0-9]+/)
        end
      end

      it 'has description filled' do
        emoji_data.each do |data|
          expect(data.description).not_to be_empty
        end
      end

      it 'has qualification filled' do
        emoji_data.each do |data|
          expect(data.qualification).not_to be_empty
        end
      end

      it 'has group_category filled' do
        emoji_data.each do |data|
          expect(data.group_category).not_to be_empty
        end
      end
    end
  end

  describe '#load!' do
    before do
      TanukiEmoji.index.reset!(reload: false)

      described_class.new(index: TanukiEmoji.index).load!
    end

    it 'adds emojis' do
      emoji = TanukiEmoji.find_by_alpha_code('smiling_face')

      expect(emoji.codepoints).to eq '☺️'
      expect(emoji.unicode_version).to eq '6.0'
      expect(emoji.description).to eq 'smiling face'
      expect(emoji.category).to eq 'Smileys & Emotion'
      expect(emoji.codepoints_alternates.first).to eq '☺'

      emoji = TanukiEmoji.find_by_alpha_code('palm_down_hand_medium_dark_skin_tone')

      expect(emoji.codepoints).to eq '🫳🏾'
      expect(emoji.unicode_version).to eq '14.0'
      expect(emoji.description).to eq 'palm down hand: medium-dark skin tone'
      expect(emoji.category).to eq 'People & Body'
      expect(emoji.codepoints_alternates.first).to be_nil
    end

    it 'removes trailing underscores' do
      expect(TanukiEmoji.find_by_alpha_code('a_button_blood_type')).not_to be_nil
      expect(TanukiEmoji.find_by_alpha_code('b_button_blood_type')).not_to be_nil
      expect(TanukiEmoji.find_by_alpha_code('ab_button_blood_type')).not_to be_nil
      expect(TanukiEmoji.find_by_alpha_code('flag_myanmar_burma')).not_to be_nil
    end

    it 'remaps keycap_* and keycap_#' do
      expect(TanukiEmoji.find_by_alpha_code('keycap_*')).to be_nil
      expect(TanukiEmoji.find_by_alpha_code('keycap_asterisk')).not_to be_nil
      expect(TanukiEmoji.find_by_alpha_code('keycap_#')).to be_nil
      expect(TanukiEmoji.find_by_alpha_code('keycap_hash')).not_to be_nil
    end
  end
end
